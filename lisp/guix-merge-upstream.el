;;; guix-merge-upstream.el --- Misc function for Emacs Guix  -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Oleg Pykhalov

;; Author: Oleg Pykhalov <go.wigust@gmail.com>
;; Keywords: local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides additional functionality in addition to Emacs
;; Guix.

;;; Code:

(defun guix-mu-next ()
  (interactive)
  (forward-sexp)
  (forward-char 2))

(defun guix-mu-prev ()
  (interactive)
  (backward-sexp))

(defun guix-mu-other-next ()
  (interactive)
  (other-window 1)
  (forward-sexp)
  (forward-char 2)
  (other-window 1))

(defun guix-mu-other-prev ()
  (interactive)
  (other-window 1)
  (backward-sexp)
  (other-window 1))

(defun guix-mu-kill-sexp ()
  (interactive)
  (kill-sexp)
  (let ((point (point)))
    (delete-region point (+ point 2))))

(defun guix-mu-other ()
  (interactive)
  (other-window 1))

(defun guix-mu-yank ()
  (interactive)
  (yank)
  (newline 2)
  (backward-sexp))

(defun guix-mu-copy ()
  (interactive)
  (guix-mu-kill-sexp)
  (other-window 1)
  (guix-mu-yank)
  (other-window 1))

(defun guix-mu-commit ()
  (interactive)
  (save-buffer)
  (let ((var (save-excursion
               (forward-char)
               (forward-sexp)
               (forward-char)
               (thing-at-point 'symbol)))
        (filename (buffer-file-name)))
    (call-interactively #'vc-next-action)
    (call-interactively #'vc-next-action)
    (insert "gnu: Add " var)
    (newline 2)
    (insert (format "* %s (%s): New public variable."
                    (f-relative filename
                                (f-expand (vc-git-root filename)))
                    var))
    (log-edit-done)))

(defun guix-mu-copy-and-commit ()
  (interactive)
  (guix-mu-kill-sexp)
  (other-window 1)
  (guix-mu-yank)
  (guix-mu-commit)
  (other-window 1))

(defun guix-mu-revert ()
  (interactive)
  (yes-or-no-p "Revert all changes ? ")
  (revert-buffer nil t)
  (other-window 1)
  (revert-buffer nil t)
  (other-window 1))

(defun guix-mu-hide-all ()
  (interactive)
  (let ((hs-hide-all-non-comment-function nil))
    (hs-hide-all)))

(defun guix-mu-search (package)
  "Search for a PACKAGE and return it."
  (interactive)
  (goto-char (point-min))
  (search-forward (concat "(define-public " package))
  (beginning-of-line)
  package)

(defun guix-mu-kill ()
  "Kill sexp and delete blank lines."
  (kill-sexp)
  (delete-blank-lines))

(defun guix-commit (package message comment)
  "Commit a PACKAGE with MESSAGE and COMMENT."
  (let* ((filename (buffer-file-name (current-buffer)))
         (relative (f-relative filename
                               (f-expand (vc-git-root filename)))))
    (call-interactively #'vc-next-action)
    (call-interactively #'vc-next-action)
    (insert (format "%s: %s\n\n"
                    (car (split-string relative "/")) message)
            (format "* %s (%s): %s" relative package comment))
    (call-interactively #'fill-paragraph)
    (log-edit-done)))

(defun guix-mu-foo ()
  (interactive)
  (let ((package (thing-at-point 'symbol)))
    (beginning-of-line)
    (kill-line 1)
    (with-current-buffer (get-buffer "emacs.scm<wigust>")
      (guix-mu-search package)
      (guix-mu-kill)
      (save-buffer)
      (guix-commit package
                   (format "Merge %s to Guix package collection."
                           package)
                   (format "Merge %s to Guix package collection."
                           package)))
    (with-current-buffer (get-buffer "emacs.scm<gnu>")
      (goto-char (point-max))
      (delete-blank-lines)
      (insert "\n")
      (yank)
      (save-buffer)
      (guix-commit package
                   (format "Add %s." package)
                   "New public variable."))
    (switch-to-buffer (get-buffer "*shell guix*"))
    (insert "env PAGER= git log --oneline --max-count=1")
    (comint-send-input)
    (insert "./pre-inst-env env GUIX_PACKAGE_PATH= guix build --no-grafts "
            package)
    (comint-send-input)))

(defun guix-commit-fix-linter ()
  (interactive)
  (save-buffer)
  (let ((var (save-excursion
               (geiser-syntax--pop-to-top)
               (forward-char)
               (forward-sexp)
               (forward-char)
               (thing-at-point 'symbol)))
        (filename (buffer-file-name)))
    (call-interactively #'vc-next-action)
    (call-interactively #'vc-next-action)
    (insert "wigust: " var ": Fix linter warning.")
    (call-interactively #'fill-paragraph)
    (newline 2)
    (insert (format "* %s (%s): Fix linter warning."
                    (f-relative filename
                                (f-expand (vc-git-root filename)))
                    var))
    (log-edit-done)))

;; (local-set-key (kbd "<f5>") (lambda () (interactive) (guix-mu-foo)))
;; (local-set-key (kbd "<f5>") (lambda () (interactive) (switch-to-buffer "*Shell Command Output*")))

;; git -C /home/natsu/src/guix-wigust reset --hard HEAD~ && git reset --hard HEAD~

;; (defhydra hydra-guix-mu () "merge upstream"
;;   ("C" guix-mu-copy-and-commit "copy and commit")
;;   ("N" guix-mu-other-next "other next")
;;   ("P" guix-mu-other-prev "other prev")
;;   ("Y" guix-mu-copy "copy")
;;   ("c" guix-mu-commit "commit")
;;   ("k" guix-mu-kill-sexp "kill-sexp")
;;   ("n" guix-mu-next "next")
;;   ("o" guix-mu-other "other")
;;   ("p" guix-mu-prev "prev")
;;   ("q" nil "quit")
;;   ("r" guix-mu-revert "revert")
;;   ("u" undo "undo")
;;   ("y" guix-mu-yank "yank"))

;; (global-set-key (kbd "<f5>") #'hydra-guix-mu/body)

(provide 'guix-merge-upstream)
;;; guix-merge-upstream.el ends here
