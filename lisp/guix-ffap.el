;;; emacs-guix-ffap.el --- Misc function for Emacs Guix  -*- lexical-binding: t; -*-

;; Copyright (C) 2018  Oleg Pykhalov

;; Author: Oleg Pykhalov <go.wigust@gmail.com>
;; Keywords: local

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This package provides additional functionality in addition to Emacs
;; Guix.

;;; Code:

(defcustom guix-package-regexp
  (rx (one-or-more (or alphanumeric "-" ".")))
  "Regexp matching Guix store path."
  :type 'regexp
  :group 'guix)

(defcustom guix-package-source-regexp
  (concat guix-package-regexp (rx ".tar" (zero-or-more ".gz")))
  "Regexp matching Guix package source path."
  :type 'regexp
  :group 'guix)

(defcustom guix-store-path-regexp
  (concat guix-store-directory "/" guix-hash-regexp
          "-" guix-package-regexp (rx line-end))
  "Regexp matching Guix store path."
  :type 'regexp
  :group 'guix)

(defcustom guix-store-path-package-source-regexp
  (concat guix-store-directory "/" guix-hash-regexp
          "-" guix-package-source-regexp (rx line-end))
  "Regexp matching Guix store path to package source."
  :type 'regexp
  :group 'guix)

(defun guix-ffap-store-package-source-path-p (filename)
  "If FILENAME matches `guix-store-path-regexp', return it."
  (when (string-match-p guix-store-path-package-source-regexp filename)
    filename))

(defun guix-ffap-store-path-p (filename)
  "If FILENAME matches `guix-store-path-regexp', return it."
  (when (string-match-p guix-store-path-regexp filename) filename))

(provide 'emacs-guix-ffap)
;;; emacs-guix-ffap.el ends here
