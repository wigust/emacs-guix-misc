;;; guix.scm --- Additional functions for Emacs Guix

;; Copyright © 2018 Oleg Pykhalov <go.wigust@gmail.com>

;; Emacs-Guix-Misc is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; Emacs-Guix-Misc is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with Emacs-Guix-Misc.
;; If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains Guix package for development version of
;; Emacs-Guix-Misc.  To build or install, run:
;;
;;   guix build --file=guix.scm
;;   guix package --install-from-file=guix.scm

;;; Code:

(use-modules ((guix licenses) #:prefix license:)
             (guix build utils)
             (guix build-system emacs)
             (guix gexp)
             (guix git-download)
             (guix packages)
             (ice-9 popen)
             (ice-9 rdelim))

(define %source-dir (dirname (current-filename)))

(define (git-output . args)
  "Execute 'git ARGS ...' command and return its output without trailing
newspace."
  (with-directory-excursion %source-dir
    (let* ((port   (apply open-pipe* OPEN_READ "git" args))
           (output (read-string port)))
      (close-port port)
      (string-trim-right output #\newline))))

(define (current-commit)
  (git-output "log" "-n" "1" "--pretty=format:%H"))

(let ((commit (current-commit)))
  (package
    (name "emacs-guix-misc")
    (version (string-append "0.0.1" "-" (string-take commit 7)))
    (source (local-file %source-dir
                        #:recursive? #t
                        #:select? (git-predicate %source-dir)))
    (build-system emacs-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'chdir-elisp
           ;; Elisp directory is not in root of the source.
           (lambda _
             (chdir "lisp"))))))
    (home-page "https://github.com/wigust/emacs-guix-misc")
    (synopsis "Additional functions for Emacs Guix")
    (description
     "This package provides an additional functionality for Emacs Guix.")
    (license license:gpl3+)))

;;; guix.scm ends here
